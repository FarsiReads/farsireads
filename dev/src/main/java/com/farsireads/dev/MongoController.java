package com.farsireads.dev;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MongoController {

    private BookRepository repository;

    public MongoController(BookRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/mongolist")
    public String mangoPrint() {
        return repository.findFirstByTitle("bible").toString();
    }

    @RequestMapping(value ="/test")
    public String test() {
        return "template1";
    }
}
