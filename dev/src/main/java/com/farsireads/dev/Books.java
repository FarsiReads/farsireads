package com.farsireads.dev;

import org.springframework.data.annotation.Id;


public class Books {

    @Id
    private String id;
    private String title;
    private String isbn;

    public Books(String id, String title, String isbn) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return String.format(
                "Book[id=%s, Title='%s', ISBN='%s']",
                id, title, isbn);
    }
}

