package com.farsireads.dev;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface BookRepository extends MongoRepository<Books, String> {

    Books findFirstByTitle(String title);

    @Query("{title:'?0'}")
    List<Books> findBooksByTitle(String title);

}
